%{?mingw_package_header}

#%%global snapshot_date 20160723
#%%global snapshot_rev 65a0c3298db7cc5cbded63259663cb29e4780a56
#%%global snapshot_rev_short %(echo %snapshot_rev | cut -c1-6)
#%%global branch v5.x

#%%global pre rc2

Name:           mingw-crt
Version:        9.0.0
Release:        3
Summary:        MinGW Windows cross-compiler runtime

License:        Public Domain and ZPLv2.1
URL:            http://mingw-w64.sourceforge.net/
%if 0%{?snapshot_date}
# To regenerate a snapshot:
# Use your regular webbrowser to open https://sourceforge.net/p/mingw-w64/mingw-w64/ci/%{snapshot_rev}/tarball
# This triggers the SourceForge instructure to generate a snapshot
# After that you can pull in the archive with:
# spectool -g mingw-crt.spec
Source0:        http://sourceforge.net/code-snapshots/git/m/mi/mingw-w64/mingw-w64.git/mingw-w64-mingw-w64-%{snapshot_rev}.zip
%else
Source0:        http://downloads.sourceforge.net/mingw-w64/mingw-w64-v%{version}%{?pre:-%{pre}}.tar.bz2
%endif
# Fix build failure with gcc11
Patch0:         mingw-crt_gcc11.patch

BuildArch:      noarch

BuildRequires:  make

BuildRequires:  mingw32-filesystem >= 95
BuildRequires:  mingw32-binutils
BuildRequires:  mingw32-headers
BuildRequires:  mingw32-gcc

BuildRequires:  mingw64-filesystem >= 95
BuildRequires:  mingw64-binutils
BuildRequires:  mingw64-headers
BuildRequires:  mingw64-gcc


%description
MinGW Windows cross-compiler runtime, base libraries.


%package -n mingw32-crt
Summary:        MinGW Windows cross-compiler runtime for the win32 target
Obsoletes:      mingw32-runtime < 3.18-7
Provides:       mingw32-runtime = 3.18-7
Requires:       mingw32-filesystem >= 95

%description -n mingw32-crt
MinGW Windows cross-compiler runtime, base libraries for the win32 target.

%package -n mingw64-crt
Summary:        MinGW Windows cross-compiler runtime for the win64 target
Obsoletes:      mingw64-runtime < 1.0-0.3.20100914
Provides:       mingw64-runtime = 1.0-0.3.20100914
Requires:       mingw64-filesystem >= 95

%description -n mingw64-crt
MinGW Windows cross-compiler runtime, base libraries for the win64 target.


%prep
%if 0%{?snapshot_date}
rm -rf mingw-w64-v%{version}
mkdir mingw-w64-v%{version}
cd mingw-w64-v%{version}
unzip %{S:0}
%autosetup -p1 -D -T -n mingw-w64-v%{version}/mingw-w64-mingw-w64-%{snapshot_rev}
%else
%autosetup -p1 -n mingw-w64-v%{version}%{?pre:-%{pre}}
%endif


%build
pushd mingw-w64-crt
    # Filter out -fstack-protector and -lssp from LDFLAGS as libssp is not yet potentially built with the bootstrap gcc
    MINGW32_LDFLAGS="`echo %{mingw32_ldflags} | sed 's|-fstack-protector||' | sed 's|-lssp||'`"
    MINGW64_LDFLAGS="`echo %{mingw64_ldflags} | sed 's|-fstack-protector||' | sed 's|-lssp||'`"
    MINGW64_CONFIGURE_ARGS="--disable-lib32"
    %mingw_configure
    %mingw_make_build
popd


%install
pushd mingw-w64-crt
    %mingw_make_install DESTDIR=%{buildroot}
popd

# Dunno what to do with these files
rm -rf %{buildroot}%{mingw32_includedir}/*.c
rm -rf %{buildroot}%{mingw64_includedir}/*.c


%files -n mingw32-crt
%license COPYING DISCLAIMER DISCLAIMER.PD
%{mingw32_libdir}/*

%files -n mingw64-crt
%license COPYING DISCLAIMER DISCLAIMER.PD
%{mingw64_libdir}/*


%changelog
* Tue Feb 20 2024 yinhongchang <yinhongchang@kylinsec.com.cn> - 9.0.0-3
- Update to 9.0.0

* Wed Nov 04 2020 Zhiyi Weng <zhiyi@iscas.ac.cn> - 6.0.0-4
- Update to 6.0.0

* Thu Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.0.4-3
- Package init
